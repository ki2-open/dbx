import { observable, computed, action } from "mobx";
import { exist } from "@ki2/utils";
import type {
  IViewable,
  FilterFuncType,
  IAbstractViewer,
  IFunctionFilterViewer,
} from "@ki2/dbx-api";

export type { IViewable, IViewableDB, FilterFuncType } from "@ki2/dbx-api";

export abstract class AbstractViewer<M> implements IAbstractViewer<M> {
  @observable protected source: IViewable<M>;

  constructor(source: IViewable<M>) {
    this.source = source;
  }

  @action setSource(source: IViewable<M>): void {
    this.source = source;
  }

  protected abstract _filter(): Array<M>;

  @computed get view(): Array<M> {
    return this._filter();
  }
}

export class FunctionFilterViewer<M>
  extends AbstractViewer<M>
  implements IFunctionFilterViewer<M>
{
  @observable protected _func: FilterFuncType<M> | undefined;

  constructor(source: IViewable<M>, func?: FilterFuncType<M>) {
    super(source);
    this._func = func;
  }

  @action setFunction(func: FilterFuncType<M>) {
    this._func = func;
  }

  protected override _filter(): Array<M> {
    if (exist(this._func)) {
      return this.source.view.filter(this._func);
    }
    return [];
  }
}
