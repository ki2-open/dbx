import { observable, action, computed, ObservableMap } from "mobx";
import { exist } from "@ki2/utils";
import { DBStoreError } from "@ki2/dbx-api";
import type { IDBStore } from "@ki2/dbx-api";

import { DBItem } from ".";

export { DBStoreError } from "@ki2/dbx-api";

export class DBStore<M> implements IDBStore<M> {
  @observable protected map: ObservableMap<string, M>;

  constructor() {
    this.map = new ObservableMap<string, M>();
  }

  @action set(id: string | null | undefined, data: M) {
    if (exist(id)) {
      this.map.set(id, data);
    }
  }

  @action has(id: string | null | undefined): boolean {
    if (exist(id)) {
      return this.map.has(id);
    }
    return false;
  }

  get(id: string | null | undefined): M | null {
    if (this.has(id)) {
      return this.map.get(id);
    }
    return null;
  }

  uget(id: string | null | undefined): M {
    const item = this.get(id);
    if (exist(item)) {
      return item;
    }
    throw new DBStoreError(`item with "${id}" not found.`);
  }

  delete(id: string | null | undefined): boolean {
    if (this.has(id)) {
      return this.map.delete(id);
    }
    return false;
  }

  @action clear(): void {
    this.map.clear();
  }

  @computed get view(): Array<M> {
    return Array.from(this.map.values());
  }

  @computed get hasData(): boolean {
    return this.view.length > 0;
  }

  @computed get isEmpty(): boolean {
    return !this.hasData;
  }

  react(id: string): DBItem<M> {
    return new DBItem<M>(this, id);
  }
}
