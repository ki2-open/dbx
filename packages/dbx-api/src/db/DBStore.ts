import type { IViewableDB } from "../viewers";
import type { IDBItem } from "./DBItem";

export interface IDBStore<M> extends IViewableDB<M> {
  clear(): void;
  react(id: string): IDBItem<M>;

  readonly hasData: boolean;
  readonly isEmpty: boolean;
}
