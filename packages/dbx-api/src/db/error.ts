export class DBStoreError extends Error {
  constructor(message = "") {
    super(message);
    this.name = "DBStoreError";
  }
}
