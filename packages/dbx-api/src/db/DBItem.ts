export interface IDBItem<M> {
  id: string | null;

  readonly item: M | null;
  getItem(): Promise<M>;
}
