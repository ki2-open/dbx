export interface IViewable<M> {
  readonly view: Array<M>;
}

export interface IViewableDB<M> extends IViewable<M> {
  set: (id: string | null | undefined, data: M) => void;
  has: (id: string | null | undefined) => boolean;
  get: (id: string | null | undefined) => M | null;
  uget: (id: string | null | undefined) => M;
}
