import type { IViewable } from ".";

export interface IAbstractViewer<M> extends IViewable<M> {
  setSource(source: IViewable<M>): void;
}

export type FilterFuncType<M> = (item: M) => boolean;

export interface IFunctionFilterViewer<M> extends IAbstractViewer<M> {
  setFunction(func: FilterFuncType<M>): void;
}
