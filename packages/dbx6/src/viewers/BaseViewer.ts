import { makeObservable, observable, action, computed } from "mobx";
import { exist } from "@ki2/utils";
import type {
  IViewable,
  FilterFuncType,
  IAbstractViewer,
  IFunctionFilterViewer,
} from "@ki2/dbx-api";

export type { IViewable, IViewableDB, FilterFuncType } from "@ki2/dbx-api";

export abstract class AbstractViewer<M> implements IAbstractViewer<M> {
  protected source: IViewable<M>;

  constructor(source: IViewable<M>) {
    this.source = source;
    makeObservable<AbstractViewer<M>, "source">(this, {
      source: observable,
      setSource: action,
      view: computed,
    });
  }

  setSource(source: IViewable<M>): void {
    this.source = source;
  }

  protected abstract _filter(): Array<M>;

  get view(): Array<M> {
    return this._filter();
  }
}

export class FunctionFilterViewer<M>
  extends AbstractViewer<M>
  implements IFunctionFilterViewer<M>
{
  protected _func: FilterFuncType<M> | undefined;

  constructor(source: IViewable<M>, func?: FilterFuncType<M>) {
    super(source);
    this._func = func;
    makeObservable<FunctionFilterViewer<M>, "_func">(this, {
      _func: observable,
      setFunction: action,
    });
  }

  setFunction(func: FilterFuncType<M>) {
    this._func = func;
  }

  protected override _filter(): Array<M> {
    if (exist(this._func)) {
      return this.source.view.filter(this._func);
    }
    return [];
  }
}
