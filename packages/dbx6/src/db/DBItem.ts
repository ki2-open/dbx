import { makeObservable, computed, observable, when } from "mobx";
import { exist } from "@ki2/utils";
import type { IDBItem } from "@ki2/dbx-api";

import type { DBStore } from ".";

export class DBItem<M> implements IDBItem<M> {
  protected db: DBStore<M>;
  protected _id: string | null;

  constructor(db: DBStore<M>);
  constructor(db: DBStore<M>, id: string | null);
  constructor(db: DBStore<M>, id: string | undefined | null = null) {
    this.db = db;
    this._id = id;
    makeObservable<DBItem<M>, "db" | "_id">(this, {
      db: observable,
      _id: observable,
      id: computed,
      item: computed,
    });
  }

  get id(): string | null {
    return this._id;
  }

  set id(v: string | null) {
    this._id = v;
  }

  async getItem() {
    await when(() => exist(this.item));
    return this.item;
  }

  get item(): M | null {
    if (this.db.hasData && exist(this.id)) {
      return this.db.get(this.id);
    }
    return null;
  }
}
